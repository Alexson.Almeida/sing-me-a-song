# -sing-me-a-song-testes
<p align="center">
  <img  width=300px src="https://user-images.githubusercontent.com/97575616/182211440-67b78510-bcd0-498f-b0d4-f65e2717201e.png"

</p>
<h1 align="center">
  Sing me a song
</h1>
<div align="center">

  <h3>Built With</h3>

  <img src="https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white" height="30px"/>
  <img src="https://img.shields.io/badge/Prisma-3982CE?style=for-the-badge&logo=Prisma&logoColor=white" height="30px"/>
  <img src="https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white" height="30px"/>
  <img src="https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white" height="30px"/>  
  <img src="https://img.shields.io/badge/Express.js-404D59?style=for-the-badge&logo=express.js&logoColor=white" height="30px"/>
  <img src="https://img.shields.io/badge/Jest-323330?style=for-the-badge&logo=Jest&logoColor=white" height="30px"/>
  <img src="https://i.ibb.co/WHZ1BCR/cypress.png" height="30px"/>

  <!-- Badges source: https://dev.to/envoy_/150-badges-for-github-pnk -->
</div>

<br/>

# Description
<p align="justify">
<b>Sing me a song</b> is an application for anonymous song recommendation. The more people like a recommendation, the more likely it is to be recommended to others.

</p>

</br>

## Features

-   Create a recommendation music
-   Upvote and downvote each music 
-   Get a random music 
-   Get a specific top musics based in number of votes                                                                                  

</br>

## API Reference

### RECOMMENDATIONS

### Create a recommendation

```http
POST /recommendation
```

#### Request:

####

| Body   | Type       | Description             |
| :----- | :--------- | :---------------------- |
| `name`           | `string` | **Required**. recommendation name      |
| `youtubeLink`         | `string` | **Required**. recommendation url          |

`youtubeLinkRegex = /^(https?\:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$/;`

#


### Get recommendations

```http
GET /recommendations
```
#### Response:

```json
[
  {
    "id": 3,
    "name": "Numb",
    "youtubeLink": "https://www.youtube.com/watch?v=mRXKnG2eugU",
    "score": 0
  },
  {
    "id": 2,
    "name": "Feels Like Home (Radio Edit)",
    "youtubeLink": "https://www.youtube.com/watch?v=5CY1vbxP4Jo",
    "score": 0
  },
  {
    "id": 1,
    "name": "All That Really Matters",
    "youtubeLink": "https://www.youtube.com/watch?v=3gxxW5NqICc",
    "score": 0
  }
]
```

#

### Get a recommendation by id

```http
GET /recommendations/:id
```
#### Request:

| Params      | Type      | Description           |
| :---------- | :-------- | :-------------------- |
| `id` | `number` | **Required**.|

####

#### Response:

```json
{
  "id": 1,
  "name": "All That Really Matters",
  "youtubeLink": "https://www.youtube.com/watch?v=3gxxW5NqICc",
  "score": 0
}
```

#

### Post a upvote for a recommendation

```http
POST /recommendations/:id/upvote
```
#### Request:

| Params      | Type      | Description           |
| :---------- | :-------- | :-------------------- |
| `id` | `number` | **Required**.|

####

#

### Post a downvote for a recommendation

```http
POST /recommendations/:id/downvote
```
#### Request:

| Params      | Type      | Description           |
| :---------- | :-------- | :-------------------- |
| `id` | `number` | **Required**.|

####

#

### Get a top musics recommendations - based in amount params - with decreasing score

```http
GET /recommendations/top/:amount
```
#### Request:

| Params      | Type      | Description           |
| :---------- | :-------- | :-------------------- |
| `amount` | `number` | **Required**.|

####

`example = /recommendations/top/2`

#### Response:

```json
[
  {
    "id": 1,
    "name": "All That Really Matters",
    "youtubeLink": "https://www.youtube.com/watch?v=3gxxW5NqICc",
    "score": 1
  },
  {
    "id": 2,
    "name": "Feels Like Home (Radio Edit)",
    "youtubeLink": "https://www.youtube.com/watch?v=5CY1vbxP4Jo",
    "score": 0
  }
]
```

#

### Get a random recommendation music

```http
POST /recommendations/random
```

#### Response:

```json
{
  "id": 1,
  "name": "All That Really Matters",
  "youtubeLink": "https://www.youtube.com/watch?v=3gxxW5NqICc",
  "score": 1
}
```

#


## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

<b>Back-end .env file</b>

`DATABASE_URL = postgres://UserName:Password@Hostname:5432/DatabaseName?schema=public`

`PORT = number #recommended:5000`

`MODE=test`

<b>Back-end .env.test file</b>

`DATABASE_URL = postgres://UserName:Password@Hostname:5432/DatabaseName-tests?schema=public`

`PORT = number #recommended:5000`

`MODE=test`

<b>Front-end .env file</b>

`REACT_APP_API_BASE_URL=http://`


</br>

## Run locally

Clone the project

```bash
  git clone https://gitlab.com/Athetoz/sing-me-a-song/-/tree/master
```

Go to the project directory

```bash
  cd sing-me-a-song
````

Go to the back-end directory

```bash
  cd back-end
```

Install the dependencies

```bash
  npm install
```

Go to the .env file and make the changes on the variable DATABASE_URL, passing in the string your postgres user and the password

Run the project

```bash
  npm run dev
```

Open a new terminal to generate the prisma client

After that, generate the client

```bash
  npx prisma generate
```

Left prisma work with the migrations of database

```bash
  npx prisma migrate dev
```

Return to the project directory

```bash
  cd ../
````

Go to the front-end directory

```bash
  cd front-end
```

Install the dependencies

```bash
  npm install
```

Run the project

```bash
  npm start
```

## Run for production (Docker)

Clone the project

```bash
  git clone https://gitlab.com/Athetoz/sing-me-a-song/-/tree/master
```

Go to the project directory

```bash
  cd sing-me-a-song
```

Go to the back-end directory

```bash
  cd back-end
```
Go to the .env file and make the changes on the variable DATABASE_URL, leaving the string like this: postgres://postgres:123@postgres:5432/singme?
You can make changes about the postegres user and the password, but you'll need make the changes also in the docker-compose file

Run the docker-compose file

```Docker
docker-compose up
```

Come back to the root project directory

```bash
  cd ../
```

Go to front-end directory
```bash
  cd front-end
```

Run the docker-compose file

```Docker
docker-compose up
```
